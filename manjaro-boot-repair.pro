#/*****************************************************************************
# * manjaro-boot-repair.pro
# *****************************************************************************
# * Copyright (C) 2020 Manjaro Developers
# *
# * Authors: Adrian
# *          Manjaro Linux <https://manjaro.org>
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * Manjaro Boot Repair is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with Manjaro Boot Repair.  If not, see <https://www.gnu.org/licenses/>.
# **********************************************************************/

#-------------------------------------------------
#
# Project created by QtCreator 2014-04-02T18:30:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = manjaro-boot-repair
TEMPLATE = app

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
# CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

DEFINES += QT_DEPRECATED_WARNINGS

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

        help.path = $$PREFIX/share/doc/manjaro-boot-repair/help/
        help.files = help/manjaro-boot-repair.html
        
        icons.path = $$PREFIX/share/icons/hicolor/scalable/apps/
        icons.files = manjaro-boot-repair.png
        
        desktop.path = $$PREFIX/share/applications/
        desktop.files = "manjaro-boot-repair.desktop"
        
        translation.path = $$PREFIX/share/manjaro-boot-repair/locale/
        translation.files = translations/*.qm

        INSTALLS += target  desktop help icons translation
}


SOURCES += main.cpp \
    mainwindow.cpp \
    about.cpp \
    cmd.cpp

HEADERS  += \
    version.h \
    mainwindow.h \
    about.h \
    cmd.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS += translations/manjaro-boot-repair_am.ts \
                translations/manjaro-boot-repair_ar.ts \
                translations/manjaro-boot-repair_bg.ts \
                translations/manjaro-boot-repair_ca.ts \
                translations/manjaro-boot-repair_cs.ts \
                translations/manjaro-boot-repair_da.ts \
                translations/manjaro-boot-repair_de.ts \
                translations/manjaro-boot-repair_el.ts \
                translations/manjaro-boot-repair_es.ts \
                translations/manjaro-boot-repair_et.ts \
                translations/manjaro-boot-repair_eu.ts \
                translations/manjaro-boot-repair_fa.ts \
                translations/manjaro-boot-repair_fi.ts \
                translations/manjaro-boot-repair_fil_PH.ts\
				translations/manjaro-boot-repair_fr.ts \
                translations/manjaro-boot-repair_he_IL.ts \
                translations/manjaro-boot-repair_hi.ts \
                translations/manjaro-boot-repair_hr.ts \
                translations/manjaro-boot-repair_hu.ts \
                translations/manjaro-boot-repair_id.ts \
                translations/manjaro-boot-repair_is.ts \
                translations/manjaro-boot-repair_it.ts \
                translations/manjaro-boot-repair_ja.ts \
                translations/manjaro-boot-repair_kk.ts \
                translations/manjaro-boot-repair_ko.ts \
                translations/manjaro-boot-repair_lt.ts \
                translations/manjaro-boot-repair_mk.ts \
                translations/manjaro-boot-repair_mr.ts \
                translations/manjaro-boot-repair_nb.ts \
                translations/manjaro-boot-repair_nl.ts \
                translations/manjaro-boot-repair_pl.ts \
                translations/manjaro-boot-repair_pt.ts \
                translations/manjaro-boot-repair_pt_BR.ts \
                translations/manjaro-boot-repair_ro.ts \
                translations/manjaro-boot-repair_ru.ts \
                translations/manjaro-boot-repair_sk.ts \
                translations/manjaro-boot-repair_sl.ts \
                translations/manjaro-boot-repair_sq.ts \
                translations/manjaro-boot-repair_sr.ts \
                translations/manjaro-boot-repair_sv.ts \
                translations/manjaro-boot-repair_tr.ts \
                translations/manjaro-boot-repair_uk.ts \
                translations/manjaro-boot-repair_zh_CN.ts \
                translations/manjaro-boot-repair_zh_TW.ts

RESOURCES += \
    images.qrc

