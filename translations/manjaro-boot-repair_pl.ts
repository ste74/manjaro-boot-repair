<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <source>Manjaro Boot Repair</source>
        <translation>Manjaro Naprawa rozruchu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="39"/>
        <source>Manjaro Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>Manjaro Naprawa rozruchu to narzędzie, za pomocą którego można ponownie zainstalować program rozruchowy GRUB na ESP (partycja systemowa EFI), na MBR (Master Boot Record - główny rekord rozruchowy) lub na partycji głównej. Zapewnia opcję odtworzenia pliku konfiguracyjnego GRUB oraz tworzenia kopii zapasowych i przywracania MBR lub PBR (root).</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>What would you like to do?</source>
        <translation>Co chciałbyś zrobić?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="79"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Wykonaj kopię zapasową MBR lub PBR (tylko starsze komputery)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="86"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Zainstaluj ponownie program rozruchowy GRUB na ESP, MBR lub PBR (root)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <source>Repair GRUB configuration file</source>
        <translation>Napraw plik konfiguracyjny GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Przywróć MBR lub PBR z kopii zapasowej (tylko starsza wersja rozruchowa)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <location filename="../mainwindow.cpp" line="485"/>
        <source>Select Boot Method</source>
        <translation>Wybierz metodę rozruchu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="154"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record (główny sektor rozruchowy)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="157"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <location filename="../mainwindow.ui" line="430"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Root (Partycja rozruchowa)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <location filename="../mainwindow.cpp" line="488"/>
        <source>root</source>
        <translation>root</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <location filename="../mainwindow.cpp" line="487"/>
        <source>Install on:</source>
        <translation>Zainstaluj na:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.cpp" line="486"/>
        <source>Location:</source>
        <translation>Lokalizacja:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <location filename="../mainwindow.cpp" line="496"/>
        <source>Select root location:</source>
        <translation type="Wybierz lokalizację root"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="290"/>
        <source>EFI System Partition</source>
        <translation>Partycja systemowa EFI</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="420"/>
        <source>About this application</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="423"/>
        <source>About...</source>
        <translation>O...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="472"/>
        <source>Display help </source>
        <translation>Wyświetl pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="475"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="482"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="523"/>
        <source>Cancel any changes then quit</source>
        <translation>Anuluj wszelkie zmiany, a następnie zakończ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="526"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="552"/>
        <source>Apply any changes</source>
        <translation>Zastosuj wszystkie zmiany</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="555"/>
        <location filename="../mainwindow.cpp" line="70"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="86"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>GRUB jest instalowany na urządzeniu %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <location filename="../mainwindow.cpp" line="203"/>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="394"/>
        <location filename="../mainwindow.cpp" line="419"/>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>Błąd tworzenia środowiska chroot
Proszę sprawdź wybraną lokalizację docelową.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="179"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>Plik konfiguracyjny GRUB (grub.cfg) jest przebudowywany.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="222"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Tworzenie kopii zapasowej MBR lub PBR z urządzenia %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>You are going to write the content of </source>
        <translation>Za chwilę nastąpi zapisanie zawartości </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source> to </source>
        <translation>do</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>

Are you sure?</source>
        <translation>

Jesteś pewien?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Przywracanie MBR/PBR z kopii zapasowej na urządzenie %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="313"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>Nie można znaleźć partycji systemowej EFI (ESP) na żadnym dysku systemowym. Utwórz ESP i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Select %1 location:</source>
        <translation type="Wybierz lokalizację %1"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Back</source>
        <translation>Wstecz</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="388"/>
        <source>Success</source>
        <translation>Sukces </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="389"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Manjaro Boot Repair?&lt;/b&gt;</source>
        <translation>Proces zakończył się sukcesem. &lt;p&gt;Czy chcesz opuścić Manjaro Naprawa rozruchu?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="395"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>Proces zakończony. Wystąpiły błędy.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="412"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="420"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Niestety, nie można otworzyć kontenera LUKS %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="495"/>
        <source>Select GRUB location</source>
        <translation>Wybierz lokalizację GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="507"/>
        <source>Select Item to Back Up</source>
        <translation>Wybierz element do utworzenia kopii zapasowej</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="514"/>
        <source>Select Item to Restore</source>
        <translation>Wybierz element do przywrócenia</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="525"/>
        <source>Select backup file name</source>
        <translation>Wybierz nazwę pliku kopii zapasowej</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>No file was selected.</source>
        <translation>Nie wybrano pliku.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="532"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Wybierz plik kopii zapasowej MBR lub PBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="552"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="553"/>
        <source>Version: </source>
        <translation>Wersja:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="554"/>
        <source>Simple boot repair program for Manjaro Linux</source>
        <translation>Prosty program do naprawy rozruchu dla dystrybucji antiX Manjaro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>Copyright (c) Manjaro Linux</source>
        <translation>Prawa autorskie © Manjaro Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="557"/>
        <source>%1 License</source>
        <translation>%1 Licencja</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="571"/>
        <source>%1 Help</source>
        <translation>%1 Pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="598"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation type="Niestety, nie udało się zamontować partycji %1"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Dziennik zmian</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>You must run this program as root.</source>
        <translation>Musisz uruchomić ten program jako administrator. </translation>
    </message>
</context>
</TS>
