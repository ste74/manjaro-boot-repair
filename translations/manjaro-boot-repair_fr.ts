<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <source>Manjaro Boot Repair</source>
        <translation>Manjaro Réparation d&apos;amorçage</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="39"/>
        <source>Manjaro Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>Manjaro Réparation d&apos;amorçage est un utilitaire qui peut être utilisé pour réinstaller GRUB, chargeur d&apos;amorçage, sur l&apos;ESP (EFI Système Partition), le MBR (Master Boot Record) ou sur la partition racine. Il offre la possibilité de reconstruire le fichier de configuration de GRUB et de sauvegarder et restaurer le MBR ou PBR (root).</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>What would you like to do?</source>
        <translation>Que souhaitez-vous faire?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="79"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Sauvegarder le MBR ou PBR (démarrage en mode hérité/legacy uniquement)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="86"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Réinstaller le chargeur d&apos;amorçage GRUB sur ESP, MBR ou PRB (root)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <source>Repair GRUB configuration file</source>
        <translation>Réparer le fichier de configuration de GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Restaurer le MBR ou PBR de sauvegarde (démarrage en mode hérité/legacy uniquement)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <location filename="../mainwindow.cpp" line="485"/>
        <source>Select Boot Method</source>
        <translation>Sélectionner la méthode d&apos;amorçage</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="154"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="157"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <location filename="../mainwindow.ui" line="430"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Racine (Partition Boot Record)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <location filename="../mainwindow.cpp" line="488"/>
        <source>root</source>
        <translation>/ racine</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <location filename="../mainwindow.cpp" line="487"/>
        <source>Install on:</source>
        <translation>Installer dans :</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.cpp" line="486"/>
        <source>Location:</source>
        <translation>Emplacement:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <location filename="../mainwindow.cpp" line="496"/>
        <source>Select root location:</source>
        <translation>Sélectionner l&apos;emplacement de la racine :</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="290"/>
        <source>EFI System Partition</source>
        <translation>Partition système EFI</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="420"/>
        <source>About this application</source>
        <translation>À propos de cette application</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="423"/>
        <source>About...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="472"/>
        <source>Display help </source>
        <translation>Afficher l&apos;aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="475"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="482"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="523"/>
        <source>Cancel any changes then quit</source>
        <translation>Annuler toutes les modifications puis quitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="526"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="552"/>
        <source>Apply any changes</source>
        <translation>Appliquer les changements</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="555"/>
        <location filename="../mainwindow.cpp" line="70"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="86"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>GRUB est en cours d&apos;installation sur le périphérique %1.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <location filename="../mainwindow.cpp" line="203"/>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="394"/>
        <location filename="../mainwindow.cpp" line="419"/>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>Impossible d&apos;établir l&apos;environnement chroot.
Veuillez revérifier l&apos;emplacement sélectionné.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="179"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>Le fichier de configuration de GRUB (grub.cfg) est en cours de reconstruction.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="222"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Sauvegarde du MBR ou PBR à partir du périphérique %1.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>You are going to write the content of </source>
        <translation>Vous allez écrire le contenu de</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source> to </source>
        <translation>à</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>

Are you sure?</source>
        <translation>

Etes-vous sûr?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Restauration du MBR/PBR à partir de la sauvegarde du périphérique %1.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="313"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>EFI system partition (ESP) introuvable sur les disques systèmes. Veuillez créer un ESP et réessayer.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Select %1 location:</source>
        <translation>Sélectionner l&apos;emplacement de %1 :</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="388"/>
        <source>Success</source>
        <translation>Réussite de l&apos;installation</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="389"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Manjaro Boot Repair?&lt;/b&gt;</source>
        <translation>Le processus s&apos;est achevé avec succès. &lt;p&gt;&lt;b&gt; Voulez-vous quitter Manjaro Réparation d&apos;amorçage?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="395"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>Processus terminé. Des erreurs se sont produites.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="412"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Entrer le mot de passe pour déverrouiller la partition chiffrée %1 :</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="420"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Désolé, impossible d&apos;ouvrir le conteneur% 1 LUKS</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="495"/>
        <source>Select GRUB location</source>
        <translation>Sélectionner l&apos;emplacement de GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="507"/>
        <source>Select Item to Back Up</source>
        <translation>Choisissez un élément à sauvegarder</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="514"/>
        <source>Select Item to Restore</source>
        <translation>Choisissez un élément à restaurer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="525"/>
        <source>Select backup file name</source>
        <translation>Sélectionnez le nom du fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>No file was selected.</source>
        <translation>Aucun fichier n&apos;a été sélectionné.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="532"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Sélectionnez le fichier de sauvegarde de MBR ou PBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="552"/>
        <source>About %1</source>
        <translation>À propos %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="553"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="554"/>
        <source>Simple boot repair program for Manjaro Linux</source>
        <translation>Simple programme de réparation d&apos;amorçage pour Manjaro Linux </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>Copyright (c) Manjaro Linux</source>
        <translation>Copyright (c) Manjaro Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="557"/>
        <source>%1 License</source>
        <translation>%1 Licence</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="571"/>
        <source>%1 Help</source>
        <translation>%1 Aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="598"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Désolé, impossible de monter la partition %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Journal des modifications</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>You must run this program as root.</source>
        <translation>Vous devez lancer cette application avec les droits d&apos;administrateur</translation>
    </message>
</context>
</TS>
