<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <source>Manjaro Boot Repair</source>
        <translation>Manjaro Reparador de Inicialização</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="39"/>
        <source>Manjaro Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>What would you like to do?</source>
        <translation>O que pretende fazer?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="79"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Cópia de segurança do MBR ou do PBR (apenas em computadores com BIOS)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="86"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Reinstalar o gerenciador de iniciação GRUB em ESP, MBR ou PBR (root)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <source>Repair GRUB configuration file</source>
        <translation>Reparar o arquivo de configuração do GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Restaurar MBR ou PBR a partir de cópia (apenas em computadores com BIOS)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <location filename="../mainwindow.cpp" line="485"/>
        <source>Select Boot Method</source>
        <translation>Selecionar método de iniciação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="154"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="157"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <location filename="../mainwindow.ui" line="430"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Root (Partition Boot Record)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <location filename="../mainwindow.cpp" line="488"/>
        <source>root</source>
        <translation>raíz (PBR)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <location filename="../mainwindow.cpp" line="487"/>
        <source>Install on:</source>
        <translation>Instalar em:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.cpp" line="486"/>
        <source>Location:</source>
        <translation>Localização:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <location filename="../mainwindow.cpp" line="496"/>
        <source>Select root location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="290"/>
        <source>EFI System Partition</source>
        <translation>Partição de Sistema EFI</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="420"/>
        <source>About this application</source>
        <translation>Sobre esta aplicação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="423"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="472"/>
        <source>Display help </source>
        <translation>Exibir ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="475"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="482"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="523"/>
        <source>Cancel any changes then quit</source>
        <translation>Cancelar alterações e sair</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="526"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="552"/>
        <source>Apply any changes</source>
        <translation>Aplicar alterações</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="555"/>
        <location filename="../mainwindow.cpp" line="70"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="86"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>O GRUB está sendo instalado em %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <location filename="../mainwindow.cpp" line="203"/>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="394"/>
        <location filename="../mainwindow.cpp" line="419"/>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>Não foi possível definir o ambiente chroot.
Por favor volte a verificar a localização selecionada.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="179"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>O arquivo de configuração do GRUB (grub.cfg) está sendo refeito</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="222"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Guardando o MBR (ou o PBR) de %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>You are going to write the content of </source>
        <translation>Vai escrever o conteúdo de </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source> to </source>
        <translation> em </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>

Are you sure?</source>
        <translation>
    
Tem a certeza?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Restaurando MBR/PBR em %1 a partir da cópia.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="313"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>Não foi encontrada a ESP (EFI System Partition) em qualquer dos discos do sistema. Criar uma ESP e voltar a tentar.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Select %1 location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Back</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="388"/>
        <source>Success</source>
        <translation>Sucesso</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="389"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Manjaro Boot Repair?&lt;/b&gt;</source>
        <translation>O processo terminou com êxito.&lt;p&gt;&lt;b&gt;Gostaria de sair do Manjaro Reparador de Inicialização?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="395"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>O processo terminou. Ocorreram erros.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="412"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="420"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Desculpe, não foi possível abrir o container %1 LUKS</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="495"/>
        <source>Select GRUB location</source>
        <translation>Selecione a localização do GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="507"/>
        <source>Select Item to Back Up</source>
        <translation>Selecione o item a copiar para segurança</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="514"/>
        <source>Select Item to Restore</source>
        <translation>Selecione o item a restaurar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="525"/>
        <source>Select backup file name</source>
        <translation>Escolha o nome do arquivo de cópia (backup).</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>No file was selected.</source>
        <translation>Não foi seleccionado qualquer arquivo.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="532"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Seleccione o arquivo de cópia (backup) MBR ou PBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="552"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="553"/>
        <source>Version: </source>
        <translation>Versão: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="554"/>
        <source>Simple boot repair program for Manjaro Linux</source>
        <translation>Programa simples de reparação da inicialização (boot) para o Manjaro Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>Copyright (c) Manjaro Linux</source>
        <translation>Direitos de Autor (c) Manjaro Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="557"/>
        <source>%1 License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="571"/>
        <source>%1 Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="598"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Relatório de alterações</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>You must run this program as root.</source>
        <translation>Tem que executar este programa como root.</translation>
    </message>
</context>
</TS>
