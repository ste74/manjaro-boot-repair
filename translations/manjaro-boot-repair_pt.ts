<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <source>Manjaro Boot Repair</source>
        <translation>Manjaro-Reparador do Arranque</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="39"/>
        <source>Manjaro Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>O Manjaro-Reparador do Arranque é um utilitário que pode ser usado para reinstalar o carregador do arranque GRUB na ESP (Partição do Sistema EFI), no MBR (Master Boot Record) ou na partição root. Ele fornece a opção de reconstruir o ficheiro de configuração do GRUB e fazer cópia de segurança e restaurar o MBR ou o PBR (root).</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>What would you like to do?</source>
        <translation>O que fazer?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="79"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Cópia de segurança do MBR ou do PBR (apenas em computadores com BIOS)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="86"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Reinstalar o carregador do arranque GRUB em ESP, MBR ou PBR (root)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <source>Repair GRUB configuration file</source>
        <translation>Refazer o ficheiro de configuração do GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Restaurar MBR ou PBR a partir de cópia (apenas em computadores com BIOS)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <location filename="../mainwindow.cpp" line="485"/>
        <source>Select Boot Method</source>
        <translation>Seleccionar método de arranque</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="154"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="157"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <location filename="../mainwindow.ui" line="430"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Raíz (Partition Boot Record)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <location filename="../mainwindow.cpp" line="488"/>
        <source>root</source>
        <translation>raíz (PBR)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <location filename="../mainwindow.cpp" line="487"/>
        <source>Install on:</source>
        <translation>Instalar em:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.cpp" line="486"/>
        <source>Location:</source>
        <translation>Localização:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <location filename="../mainwindow.cpp" line="496"/>
        <source>Select root location:</source>
        <translation>Seleccionar a localização de root (raíz):</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="290"/>
        <source>EFI System Partition</source>
        <translation>Partição de Sistema EFI</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="420"/>
        <source>About this application</source>
        <translation>Sobre esta aplicação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="423"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="472"/>
        <source>Display help </source>
        <translation>Exibir ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="475"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="482"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="523"/>
        <source>Cancel any changes then quit</source>
        <translation>Cancelar alterações e sair</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="526"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="552"/>
        <source>Apply any changes</source>
        <translation>Aplicar alterações</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="555"/>
        <location filename="../mainwindow.cpp" line="70"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="86"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>O GRUB está a ser instalado em %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <location filename="../mainwindow.cpp" line="203"/>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="394"/>
        <location filename="../mainwindow.cpp" line="419"/>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>Não foi possível definir o ambiente chroot.
Verificar novamente a localização selecionada.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="179"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>O ficheiro de configuração do GRUB (grub.cfg) está a ser refeito</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="222"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>A guardar o MBR/PBR de %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>You are going to write the content of </source>
        <translation>Vai escrever o conteúdo de </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source> to </source>
        <translation> em </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="288"/>
        <source>

Are you sure?</source>
        <translation>
    
Tem a certeza?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>A restaurar MBR/PBR em %1 a partir da cópia.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="313"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>Não foi encontrada a ESP (EFI System Partition) em qualquer dos discos do sistema. Criar uma ESP e voltar a tentar.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Select %1 location:</source>
        <translation>Seleccionar a localização de %1:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Back</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="388"/>
        <source>Success</source>
        <translation>Êxito</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="389"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Manjaro Boot Repair?&lt;/b&gt;</source>
        <translation>O processo terminou com êxito.&lt;p&gt;&lt;b&gt;Sair do Manjaro-Reparador do Arranque?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="395"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>O processo terminou. Ocorreram erros.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="412"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Introduzir a senha para desbloquear a partição encriptada %1:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="420"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Não foi possível abrir o contentor LUKS %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="495"/>
        <source>Select GRUB location</source>
        <translation>Seleccionar a localização do GRUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="507"/>
        <source>Select Item to Back Up</source>
        <translation>Seleccionar o item a copiar para segurança</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="514"/>
        <source>Select Item to Restore</source>
        <translation>Seleccionar o item a restaurar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="525"/>
        <source>Select backup file name</source>
        <translation>Escolher o nome da cópia de segurança.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="527"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>No file was selected.</source>
        <translation>Não foi seleccionado qualquer ficheiro.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="532"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Seleccionar cópia de segurança do MBR ou do PBR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="552"/>
        <source>About %1</source>
        <translation>Sobre %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="553"/>
        <source>Version: </source>
        <translation>Versão: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="554"/>
        <source>Simple boot repair program for Manjaro Linux</source>
        <translation>Programa simples de reparação do arranque para o Manjaro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>Copyright (c) Manjaro Linux</source>
        <translation>Direitos de autor (c) Manjaro Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="557"/>
        <source>%1 License</source>
        <translation>%1 Licença</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="571"/>
        <source>%1 Help</source>
        <translation>%1 Ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="598"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Não foi possível montar a partição %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Registo de alterações</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>You must run this program as root.</source>
        <translation>Este programa tem que ser executado como root.</translation>
    </message>
</context>
</TS>
